(function(){
	window.onload = function () {
		const sliderWrapper = document.querySelector('.main-content-best-choice');
		const buttonsWrapper = document.querySelector('.button-slide');
		let currentState = 'state-1';

		buttonsWrapper.addEventListener('click', function (e) {
			if(e.target.id) {
                setState(e.target.id);
                currentState = e.target.id;
            }
		});

		function setState(state) {
			sliderWrapper.classList.remove(currentState);
			sliderWrapper.classList.add(state)
		}
	};
})();